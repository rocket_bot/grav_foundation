# Grav Foundation #

Starter development template for building [Zurb Foundation 6](https://foundation.zurb.com/sites/docs/) themed [Grav CMS](https://getgrav.org) sites.

### Includes: ###

* Latest Foundation Sites 6 [source from official repository](https://github.com/zurb/foundation-sites)
* Latest [FontAwesome](http://fontawesome.io)Icons
* [Motion UI](https://foundation.zurb.com/sites/docs/motion-ui.html)
* Yarn and Gulp build system for development and production

## Installation ##

* Installing the Grav Foundation theme can be done in one of three ways. The Admin Panel (plugin) lets you easily install the theme from a list of available themes, while the GPM (Grav Package Manager) installation method enables you to quickly and easily install the theme with a simple terminal command. In addition, the manual method enables you to do so via a zip file.


### One-time Development Environment Setup ###

* Install [NodeJS and Node Package Manager](https://nodejs.org/en/) globally. 
* Run the following command-line instructions inside theme root:

`npm install -g gulp yarn`

`yarn install`

### Using and Editing ###

* #####Custom styles should be in [src/scss/style.scss](https://bitbucket.org/lizergin/grav_foundation/src/fc86a23ebfc77b4b0ee019c6c8c909137f9d90af/src/scss/style.scss?at=master&fileviewer=file-view-default), scripts in [src/js/theme.js](https://bitbucket.org/lizergin/grav_foundation/src/fc86a23ebfc77b4b0ee019c6c8c909137f9d90af/src/js/theme.js?at=master&fileviewer=file-view-default). Development changes can be automated with command-line instruction inside theme root:

`yarn run watch`

###_Build optimized, minified, auto-prefixed assets with command-line instruction inside theme root:_   

####_Yarn commands:_

`yarn run build --production`

`yarn run build`

`yarn run build-styles [-- --production]` Compiles the SCSS source code

`yarn run build-scripts [-- --production]` Compiles the Javascript source code.

`yarn run clean` Removes everything inside `assets/`

`yarn run watch` Monitors changes inside `src/*`

###License

The Grav Foundation theme is open-sourced software licensed under the [MIT](https://opensource.org/licenses/MIT) license.
###
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/P5P8M0KM)
